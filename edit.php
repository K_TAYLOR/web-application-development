<?php 
session_start();
if(!isset($_SESSION['username'])){
    header("Location: http://stuweb.cms.gre.ac.uk/~kt5509b/login.php");
}
include ('server.php');
include 'commentserver.php';
date_default_timezone_set('Europe/Copenhagen');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    
<head>
<title>Members Area</title>
<style> 
  .navbar {
  overflow: hidden;
  background-color:#d42828 ;
  background: -webkit-linear-gradient(left, #d42828 , orangered); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(right,  #d42828 , orangered); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(right,  #d42828 , orangered); /* For Firefox 3.6 to 15 */
  background: linear-gradient(to right,  #d42828 , orangered); /* Standard syntax (must be last) */
}
  position: fixed;
  top: 0;
  width: 100%;
}
 
    .main {
  padding: 16px;
  margin-top: 30px;
  height: 1500px; /* Used in this example to enable scrolling */
}
    .navbar a {
  float: right;
  display: block;
  color: white;
  text-align: center;
  padding: 14px 70px;
  text-decoration: none;
  font-size: 13px;
  font-family: Arial, Helvetica, sans-serif;       
}
    .content {
    max-width: 500px;
    margin: auto;
    background: white;
    padding: 40px;
}
         textarea {
        width: 500px;
        height: 80px;
        background-color: #fff;
        resize:: none;
    }
    .button {
    background-color: #d42828; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: block;
    font-size: 16px;
    margin: 0 auto;
    -webkit-transition-duration: 0.5s; /* Safari */
    transition-duration: 0.5s;
    cursor: pointer;
}
    .button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #d42828;
}
    .button1:hover {
    background-color: #d42828;
    color: white;
}


    
    
h1 {
background-color: ;    
color: #d42828;
text-align: center;
font-family: Arial, Helvetica, sans-serif;  
    }


    

    
</style>
    
<script src='https://www.google.com/recaptcha/api.js'></script>
    
</head>

<body>
    
<div class="navbar">
    <a href="logout.php"><b>Log out</b></a>
</div>
    
<div class="content">    
<h1>Members Area</h1>

<!--<form action="upload.php" method="POST" enctype="multipart/form-data">-->
    
<!--<this code was adapted from Youtube: mmuts>-->       
 <?php
    $cid = $_POST['cid'];
    $uid = $_POST['uid'];
    $date = $_POST['date'];
    $message = $_POST['message'];
    
    echo "<form method='POST' action='".editComments($conn)."'>
    <input type='hidden' name='cid' value='".$cid."'>
    <input type='hidden' name='uid' value='".$uid."'>
    <input type='hidden' name='date' value='".$date."'>
    <textarea name='message'>".$message."</textarea><br>
    <button type='submit' name='postb'>Edit</button>
    </form>";
    ?>
            
</div>
    
</body>

</html>
