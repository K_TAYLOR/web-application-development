<?php
    
// Variable declaration
    $username = "";
    $email = "";
    $errors = array();
    $_SESSION['success'] = "";


// Create connection / Connect to database 
$conn = mysqli_connect("mysql.cms.gre.ac.uk", "kt5509b", "kt5509b", "mdb_kt5509b");

// Check connection
if (mysqli_connect_errno($conn)) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
	exit();
}

//SIGN UP USER
//if the sign up button is pressed/clicked
if (isset($_POST['signup'])) {
    $username  = mysqli_real_escape_string ($conn, $_POST['username']);
    $email = mysqli_real_escape_string ($conn, $_POST['email']);
    $password = mysqli_real_escape_string ($conn, $_POST['password']);
  
//check if username and email is a duplicate
  $sql="SELECT * FROM users WHERE username='$username' OR email='$email'";  
  $result = mysqli_query($conn, $sql);
  $count=mysqli_num_rows($result);
  if($count > 0)
  {
       echo "This username or email is already in use";
  }else{
    if (empty($username) || empty($password)) {
     echo "A username and/or password field is empty please enter in the field below"; 
  }else  
  {
    //Register user if there are no errors
    if (count($errors) == 0){
        $hashedpassword = password_hash($password, PASSWORD_DEFAULT); //the password will be encrypted before being stored in the database
        $verification = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), -5);
        $sql = "INSERT INTO users (username, email, password, verification) 
        VALUES('$username', '$email', '$hashedpassword', '$verification')";
        
        //send verification email
        //this portion of code for the email was sourced from Evato tuts+ by Philo Hermans
        $to      = $email; // Send email to our user
        $subject = 'Signup | Verification'; // Give the email a subject 
        $message = 'Hello '.$username.', Thanks for signing up with the Greenwich community
Your account has been created, you can login with the following credentials after you have activated your account with your 5 character activation code by clicking the url below and entering it in.

------------------------
Username: '.$username.'
Activation code: '.$verification.'
------------------------

Please click this link to activate your account:
http://stuweb.cms.gre.ac.uk/~kt5509b/verification.php?email='.$email.'&verification='.$verification.'

'; // Our message above including the link
                     
$headers = 'From:noreply@stuweb.cms.gre.ac.uk' . "\r\n"; // Set from headers
mail($to, $subject, $message, $headers); // Send our email
       
        $result = mysqli_query($conn, $sql);
        
        $_SESSION['username'] = $username;
    }
    
    
    
}
}
}
?>