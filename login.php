<?php
session_start();
setcookie("username", "", time() + 86400);
if (isset($_POST{'login'})) {

include 'server.php';
    
$username = mysqli_real_escape_string ($conn, $_POST['username']);
$password = mysqli_real_escape_string ($conn, $_POST['password']);
    
    //error handlers
    //check if inputs are empty
    if (empty($username) || empty($password)) {
        //header("Location: ../index.php?login=empty");
        //exit();
         echo '<div class="statusmsg">A username and/or password field is empty please enter in the field below</div>';
    } else{
        $sql = "SELECT * FROM users WHERE username='$username' AND activation='1'";
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);
        if ($resultCheck < 1) {
            //header("Location: ../index.php?login=error");
            //exit(); 
            echo '<div class="statusmsg">The username and password that you entered did not match our records. Please double-check and try again</div>';
        } else {
            if ($row = mysqli_fetch_assoc($result)) {
                //de-hasing the password
                $hashedpasswordcheck = password_verify($password, $row['password']);
                if ($hashedpasswordcheck == false) {
                 //header("Location: ../index.php?login=error");
                 //exit(); 
                 echo '<div class="statusmsg">The username and password that you entered did not match our records. Please double-check and try again</div>';   
                } elseif ($hashedpasswordcheck == true) {
                    //log in the user here
                    $_SESSION['username'] = $row['username'];
                    $_SESSION['email'] = $row['email'];
                     header("Location: http://stuweb.cms.gre.ac.uk/~kt5509b/members.php");
                     //exit(); 
                    //echo '<div class="statusmsg">Login complete.</div>';
                }
            }
        }
    }
}

?>

 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>Login</title>
<style> 
.navbar {
  overflow: hidden;
  background-color:#d42828 ;
  background: -webkit-linear-gradient(left, #d42828 , orangered); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(right,  #d42828 , orangered); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(right,  #d42828 , orangered); /* For Firefox 3.6 to 15 */
  background: linear-gradient(to right,  #d42828 , orangered); /* Standard syntax (must be last) */
}
  position: fixed;
  top: 0;
  width: 100%;
}
 
    .main {
  padding: 16px;
  margin-top: 30px;
  height: 1500px; /* Used in this example to enable scrolling */
}
    .navbar a {
  float: left;
  display: block;
  color: white;
  text-align: center;
  padding: 14px 70px;
  text-decoration: none;
  font-size: 13px;
  font-family: Arial, Helvetica, sans-serif;       
}
    .content {
    max-width: 500px;
    margin: auto;
    background: white;
    padding: 40px;
}

 
    input[type=text], input[type=password] {
    width:   100%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    border: 3px solid #ccc;
    -webkit-transition: 0.5s;
    transition: 0.5s;
    outline: none;
}
    input[type=text]:focus {
    border: 3px solid #d42828;
}
    
    input[type=password]:focus {
    border: 3px solid #d42828;
}
    .button {
    background-color: #d42828; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: block;
    font-size: 16px;
    margin: 0 auto;
    -webkit-transition-duration: 0.5s; /* Safari */
    transition-duration: 0.5s;
    cursor: pointer;
}
    .button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #d42828;
}
    .button1:hover {
    background-color: #d42828;
    color: white;
}



    
    
h1 {
background-color: ;    
color: #d42828;
text-align: center;
font-family: Arial, Helvetica, sans-serif;  
    }


    

    
</style>
    
</head>

<body>
    
<div class="navbar">
    <a href="Login.php"><b>Home</b></a>
</div>
    
<div class="content">    
<h1>Log in</h1>

<form method="POST" action="login.php">
    <input type="text" id="username" name="username" placeholder="Username">
    <input type="password" id="password" name="password" placeholder="Password">
    <button type="submit" name="login" class="button button1">Log In</button>
    <p><small>Not yet a member? <a href="signup.php">Sign up now</a></small></p>
</form>    
    
</div>
    
</body>

</html>