<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>Success</title>
<style> 
.navbar {
  overflow: hidden;
  background-color:#ccff99 ;
  background: -webkit-linear-gradient(left, #ccff99 , #66ff33); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(right,    #ccff99 , #66ff33); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(right,   #ccff99 , #66ff33); /* For Firefox 3.6 to 15 */
  background: linear-gradient(to right,    #ccff99 , #66ff33); /* Standard syntax (must be last) */
}
  position: fixed;
  top: 0;
  width: 100%;
}
 
    .main {
  padding: 16px;
  margin-top: 30px;
  height: 1500px; /* Used in this example to enable scrolling */
}
    .navbar a {
  float: left;
  display: block;
  color: white;
  text-align: center;
  padding: 14px 70px;
  text-decoration: none;
  font-size: 13px;
  font-family: Arial, Helvetica, sans-serif;       
}
    .content {
    max-width: 500px;
    margin: auto;
    background: white;
    padding: 40px;
}

 
    input[type=text], input[type=password] {
    width:   100%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    border: 3px solid #ccc;
    -webkit-transition: 0.5s;
    transition: 0.5s;
    outline: none;
}
    input[type=text]:focus {
    border: 3px solid deepskyblue;
}
    
    input[type=password]:focus {
    border: 3px solid deepskyblue;
}
    .button {
    background-color: ; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: block;
    font-size: 16px;
    margin: 0 auto;
    -webkit-transition-duration: 0.5s; /* Safari */
    transition-duration: 0.5s;
    cursor: pointer;
}
    .button1 {
    background-color: white; 
    color: white; 
    border: 2px solid white;
    -webkit-transition: 0.5s;
    transition: 0.5s;
    outline: none;
}
    .button1:hover {
    background-color: deepskyblue;
    color: white;
}



    
    
h1 {   
color: black;
text-align: center;
font-family: Arial, Helvetica, sans-serif;  
    }

    h2 {
        color: darkgray;
        text-align: center; 
        font-family: Arial, Helvetica, sans-serif;
        font-size: 80%;
    }
    

    
</style>
    
</head>

<body>
    
<div class="navbar">
    <a href="verification.php"></a>
</div>
    
<div class="content">    
<h1>Account creation successful</h1>

<form method="post" action="SignUp.php">
    <center><img src=c9a82f13de7d8413fb8d9b4d7279cd3aa4318147_hq.gif alt="green tick" style="width:210px;height:173px;"></center>    
    
    <h2><small><center>A verification code has been sent to your email address, please follow the instructions to verify the account</center></small></h2>
    
</form>    
    
</div>
    
</body>

</html>