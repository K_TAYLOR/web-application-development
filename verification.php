<?php
include 'server.php';
$conn = mysqli_connect("mysql.cms.gre.ac.uk", "kt5509b", "kt5509b", "mdb_kt5509b") or die(mysqli_error());
 
//this code was sourced from Evato tuts+ by Philo Hermans//
if(isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['verification']) && !empty($_GET['verification'])){
 // Verify data
    $email = mysqli_real_escape_string($conn, $_GET['email']); // Set email variable
    $verification = mysqli_real_escape_string($conn, $_GET['verification']); // Set hash variable
    
    $sql = "SELECT email, verification, activation FROM users WHERE email='$email' AND verification='$verification' AND activation='0'";
    $result = mysqli_query($conn, $sql);
    $resultCheck  = mysqli_num_rows($result);
    if($resultCheck > 0){
        // We have a match, activate the account
        mysqli_query($conn,"UPDATE users SET activation='1' WHERE email='$email' AND verification='$verification' AND activation='0'");
        
        echo '<div class="statusmsg">Your account has been activated, you can now login</div>';
    }else{
        // No match -> invalid url or account has already been activated.
        echo '<div class="statusmsg">The url is either invalid or you already have activated your account.</div>';
    }
                 
}else{
    // Invalid approach
    echo '<div class="statusmsg">Invalid approach, please use the link that has been send to your email address .</div>';
}
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>Verification</title>
<style> 
.navbar {
  overflow: hidden;
  background-color:#99ccff ;
  background: -webkit-linear-gradient(left, #99ccff , #66ffff); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(right,   #99ccff , #66ffff); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(right,   #99ccff , #66ffff); /* For Firefox 3.6 to 15 */
  background: linear-gradient(to right,   #99ccff , #66ffff); /* Standard syntax (must be last) */
}
  position: fixed;
  top: 0;
  width: 100%;
}
 
    .main {
  padding: 16px;
  margin-top: 30px;
  height: 1500px; /* Used in this example to enable scrolling */
}
    .navbar a {
  float: left;
  display: block;
  color: white;
  text-align: center;
  padding: 14px 70px;
  text-decoration: none;
  font-size: 13px;
  font-family: Arial, Helvetica, sans-serif;       
}
    .content {
    max-width: 500px;
    margin: auto;
    background: white;
    padding: 40px;
}

 
    input[type=text], input[type=password] {
    width:   100%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    border: 3px solid #ccc;
    -webkit-transition: 0.5s;
    transition: 0.5s;
    outline: none;
}
    input[type=text]:focus {
    border: 3px solid deepskyblue;
}
    
    input[type=password]:focus {
    border: 3px solid deepskyblue;
}
    .button {
    background-color: ; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: block;
    font-size: 16px;
    margin: 0 auto;
    -webkit-transition-duration: 0.5s; /* Safari */
    transition-duration: 0.5s;
    cursor: pointer;
}
    .button1 {
    background-color: white; 
    color: black; 
    border: 2px solid white;
    -webkit-transition: 0.5s;
    transition: 0.5s;
    outline: none;
}
    .button1:hover {
    background-color: deepskyblue;
    color: white;
}



    
    
h1 {
background-color: ;    
color: black;
text-align: center;
font-family: Arial, Helvetica, sans-serif;  
    }

    h2 {
        color: darkgray;
        text-align: center; 
        font-family: Arial, Helvetica, sans-serif;
        font-size: 80%;
    }
    
    h3 {
        text-align: center;
    }

    
</style>
    
</head>

<body>
    
<div class="navbar">
    <a href="verification.php"></a>
</div>
    
<div class="content">    
<h1>Account Verification</h1>

<form method="post" action="login.php">
    <input  type="text" id="verification" name="verification"value="<?php echo  $verification; ?>"readonly>
    <button class="button button1">Login</button>
    <h2><small><center>Please enter your five character verification code that was sent to the email you provided and click the login button</center></small></h2>
    
</form>    
    
</div>
    
</body>

</html>
                              