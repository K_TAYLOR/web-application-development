<?php 
session_start();
if(!isset($_SESSION['username'])){
    //header("Location: http://stuweb.cms.gre.ac.uk/~kt5509b/login.php");
}
include ('server.php');
include 'commentserver.php';
include 'upload.php';
date_default_timezone_set('Europe/London');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    
<head>
<title>Members Area</title>
<style> 
  .navbar {
  overflow: hidden;
  background-color:#d42828 ;
  background: -webkit-linear-gradient(left, #d42828 , orangered); /* For Safari 5.1 to 6.0 */
  background: -o-linear-gradient(right,  #d42828 , orangered); /* For Opera 11.1 to 12.0 */
  background: -moz-linear-gradient(right,  #d42828 , orangered); /* For Firefox 3.6 to 15 */
  background: linear-gradient(to right,  #d42828 , orangered); /* Standard syntax (must be last) */
}
  position: fixed;
  top: 0;
  width: 100%;
}
 
    .main {
  padding: 16px;
  margin-top: 30px;
  height: 1500px; /* Used in this example to enable scrolling */
}
    .navbar a {
  float: right;
  display: block;
  color: white;
  text-align: center;
  padding: 14px 70px;
  text-decoration: none;
  font-size: 13px;
  font-family: Arial, Helvetica, sans-serif;       
}
    .content {
    max-width: 500px;
    margin: auto;
    background: white;
    padding: 40px;
}
    
        textarea {
        width: 500px;
        height: 80px;
        background-color: #fff;
        resize:: none;
    }
    
    button{
        width: 100px;
        height:30px;
        background-color: #d42828;
        border: none;
        color: #ffff;
        font-family: arial;
        font-weight: 400;
        cursor: pointer;
        margin-bottom: 60px;
        -webkit-transition-duration: 0.5s; /* Safari */
    transition-duration: 0.5s;   
    }
    
    .button1{
    background-color: white; 
    color: black; 
    border: 2px solid #d42828;
    }
    .button1:hover {
    background-color: #d42828;
    color: white;
}
    .comment-box{
        width: 490px;
        padding: 10px;
        margin-bottom: 20px;
        background-color: lightgrey;
        border-radius: 4px;
        position: relative;
    }
     .comment-box p {
        font-family: arial;
         font-size: 14px;
         line-height: 16px;
         color: #282828;
         font-weight: 100;
    }
    .edit-form {
    position:absolute;
    top: 0px;
    left:465px;
    -webkit-transition-duration: 0.5s; /* Safari */
    transition-duration: 0.5s;    
}
       .edit-form button {
    width: 40px;
    height: 20px;    
    background-color: #d42828; /* Green */
    color: white;
}
     .edit-form button:hover {
    background-color: #d42828;
    color: white;
}

    .delete-form {
    position:absolute;
    top: 0px;
    left: 410px;    
}
    .delete-form button {
    width: 50px;
    height: 20px;    
    background-color: #d42828; /* Green */
    color: white;
}
    .delete-form button:hover {
    background-color: #d42828;
    color: white;
}

    
h1 {
background-color: ;    
color: #d42828;
text-align: center;
font-family: Arial, Helvetica, sans-serif;  
    }


    

    
</style>
    
    
</head>

<body>
<!--<this will display the log out on the navigation bar, only to the user that is logged in/in a session state. Otherwise, for public viewing it will only have a log in section>-->    
<?php
//if a session state is non existant, a log in link will show on the nav bar//    
if (!isset($_SESSION['username']) || empty($_SESSION['username'])){  ?>    
<div class="navbar">
<a href="login.php"><small>Have an account?</small> <b>Log in</b></a>
</div>
<?php 
//however, if a session does exist, it will display the logout function only to that user and not to public viewing.                                                                   
}else{?> 
<div class="navbar">    
<a href="logout.php"><b>Log out</b></a>
</div>
<?php
}
?>    
    
<div class="content">    
<h1>Members Area</h1>

<!--<form action="upload.php" method="POST" enctype="multipart/form-data">-->
    
<!--<this code was adapted from mmuts>-->     
 <?php
    if (isset($_SESSION['username'])){
    echo "<form enctype='multipart/form-data' method='POST' action='".setComments($conn)."'>
    <input type='hidden' name='uid' value='".$_SESSION['username']."'>
    <input type='hidden' name='date' value='".date('Y-m-d H:i:s')."'>
    <textarea name='message' placeholder='Please post information on your starting and ending point of your journey. State weather you are looking to provide or request a lift, and any other details you feel need to be included...'></textarea><br>
    <button type='submit' name='postb'class='button button1'>Post</button>
    <button type='submit' name='submit'class='button button1'>Upload Image</button>
    <input type='file' name='file'>
    </form>";
    } else{
        echo"You need to be a registered member to post!
        <br><br>";
    }
    getComments($conn);
        
    ?>
            
</div>
    
</body>

</html>
