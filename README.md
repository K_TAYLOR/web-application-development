I created a website that allowed visitors to register as members using the form provided on the website. 
Once registered, this would enable them to view and respond to the message boards featured on the website.
The implementation phases were as follows: 
Account creation > Verify Account > Authentication > Member Post > Image Uploads, Member Searches > Cookies.

As a result:

•	The project has the ability to display and operate correctly on all popular web browsers, i.e Mozilla FireFox, Chrome, MS IE etc.

•	I used XHTML 1.1, HTML5, CSS and JavaScript for client side coding, and PHP for server side coding, using MySQL database server.

•	The Client side (Javascript) and Server side (PHP) scripts can be used to validate data from all forms.

•	I have produced a website that passes the W3C XHTML1.1 validation, and the website can operate with and without the use of JavaScript and CSS.
